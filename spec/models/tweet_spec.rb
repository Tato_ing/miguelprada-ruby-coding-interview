require 'rails_helper'

RSpec.describe Tweet, type: :model do
  subject(:tweet) { build(:tweet) }

  describe "validations" do
    it { is_expected.to validate_length_of(:body).is_at_most(180) }

    describe "spam?" do
      subject(:tweet) { build(:tweet, body: body, user: user) }

      let(:body) { Faker::GreekPhilosophers.quote }
      let(:user) { create(:user) }
      let(:created_at) { Time.now }

      before do
        create(:tweet, body: body, user: user, created_at: created_at)
      end

      it "is not valid" do
        expect(tweet.valid?).to eq(false)
      end

      describe "when tweet has a different body" do
        subject(:tweet) { build(:tweet, body: "abc 123", user: user) }

        it "is valid" do
          expect(tweet.valid?).to eq(true)
        end
      end

      describe "when tweet has the same body but previous was created more than 1 day ago" do
        let(:created_at) { 2.days.ago }

        it "is valid" do
          expect(tweet.valid?).to eq(true)
        end
      end
    end
  end
end
