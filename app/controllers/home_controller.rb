class HomeController < ApplicationController
  def index
    @tweets = Tweet.includes(:user, user: [:company]).distinct(:user_id).order("created_at DESC").limit(20)
  end
end
