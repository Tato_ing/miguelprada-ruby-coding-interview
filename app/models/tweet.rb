class Tweet < ApplicationRecord
  MAX_CHARACTERS_LENGTH = 180

  belongs_to :user
  validates :body, length: { maximum: MAX_CHARACTERS_LENGTH }
  validate :spam?

  scope :recent_tweets, -> { where("created_at >= ?", 24.hours.ago) }
  scope :with_body, ->(body) { where(body: body) }

  private

  def spam?
    user_tweets = user.tweets
    errors.add(:body, "looks like it's a spam") if user_tweets.recent_tweets.with_body(body).exists?
  end
end
